﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;

namespace Pn.Presentation.Mining
{

    public partial class Form1 : Form
    {
        public static string Username { get; set; }
        public static string Pool { get; set; }
        public static string Password { get; set; }
        static Pool _pool = null;
        static Work _work = null;
        static uint _nonce = 0;
        static long _maxAgeTicks = 20000 * TimeSpan.TicksPerMillisecond;
        static uint _batchSize = 100000;
        static int printcounter = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            Pool += textBox2.Text;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void GetCPUinf()
        {
            ManagementClass mo = new ManagementClass("win32_processor");
            ManagementObjectCollection moc = mo.GetInstances();
            string helloString = "===== CPU Bitcoin miner by Erik Ter - Davtyan =====";
            richTextBox1.Text += helloString + "\n";
            string proc_Id = "";
            int proc_Speed = 0;

            ManagementObjectSearcher searcher = new ManagementObjectSearcher(@"root\WMI", "SELECT * FROM MSAcpi_ThermalZoneTemperature");
            double temp = 0;
            foreach (ManagementObject obj in searcher.Get())
            {
                temp = Convert.ToDouble(obj["CurrentTemperature"].ToString());
                temp = (temp - 2732) / 10.0;
            }
            foreach (var item in moc)
            {
                proc_Id = item.Properties["processorID"].Value.ToString();
                proc_Speed = Convert.ToInt32(item.Properties["CurrentClockSpeed"].Value.ToString());
                break;
            }
            Cpu_inf.Text += proc_Id + "\n";
            Cpu_inf.Text += proc_Speed + "\n";
            Cpu_inf.Text += temp.ToString() + "\n";
        }
        void Miner()
        {
            try
            {
                GetCPUinf();
            }
            catch (Exception)
            {
                richTextBox1.Text += "Your PC doesn't support WMI to get CPU Info \n";
            }
            while (true)
            {
                try
                {
                    if (_pool == null)
                        _pool = SelectPool();
                    _work = GetWork();
                    while (true)
                    {
                        if (_work == null || _work.Age > _maxAgeTicks)
                            _work = GetWork();

                        if (_work.FindShare(ref _nonce, _batchSize))
                        {
                            SendShare(_work.Current);
                            _work = null;
                        }
                        else
                            PrintCurrentState();
                    }
                }
                catch (Exception e)
                {

                    richTextBox1.Text += "Bitcoin miner has been crashed: ";
                    richTextBox1.Text += e.Message + "\n";
                    break;

                }
            }
        }

        private void ClearConsole()
        {
            richTextBox1.Text += "==================================";
            richTextBox1.Text += "=== Erik's Bitcoin Miner ====";
            richTextBox1.Text += "==================================";
        }

        private static Pool SelectPool()
        {
            return new Pool(Pool, Username, Password);
        }

        private Work GetWork()
        {
            richTextBox1.Text += "Requesting Work from Pool...";
            return _pool.GetWork();
        }

        private void SendShare(byte[] share)
        {
            //ClearConsole();
            richTextBox1.Text += "[Success]Share found!";
            richTextBox1.Text += "Share: " + Utils.ToString(_work.Current) + "\n";
            richTextBox1.Text += "Nonce: " + Utils.ToString(_nonce) + "\n";
            richTextBox1.Text += "Hash: " + Utils.ToString(_work.Hash) + "\n";
            richTextBox1.Text += "Sending Share to Pool... \n";
            if (_pool.SendShare(share))
                richTextBox1.Text += "Share accepted!\n";
            else
                richTextBox1.Text += "Server declined the Share!\n";
        }
        private static DateTime _lastPrint = DateTime.Now;
        private void PrintCurrentState()
        {
            if (printcounter != 10)
            {
                printcounter += 1;
                return;
            }
            printcounter = 0;
            double progress = ((double)_nonce / uint.MaxValue) * 100;
            //
            TimeSpan span = DateTime.Now - _lastPrint;
            richTextBox1.Text += "Speed: " + (int)(((_batchSize) / 100) / span.TotalSeconds) + "Kh/sec " + "Share progress:" + progress.ToString("F2") + "% \n";
            _lastPrint = DateTime.Now;
        }




        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Miner();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            Username += textBox1.Text;
        }

        private void TextBox3_TextChanged(object sender, EventArgs e)
        {
            Password += textBox3.Text;
        }
    }
}

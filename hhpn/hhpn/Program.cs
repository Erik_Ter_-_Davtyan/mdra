﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace hhpn
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Arrays
            //Ordinary array, useful for indexing,not useful for searching, adding or deleting
            //usually they are used in empty databases

            int[] array = new int[7];

            //List array,useful for adding and deleting,not useful for indexing,
            //they reserve memeroy elements in case you add new or delete old element
            List<int> list = new List<int>();

            //Two-dimensional array ,they have tow indexes x & y,like array in array
            int[,] matrix = new int[7, 7];

            /*Complexity
             Indexing - ordinary array O(1),dynamic array O(1)
             Search - ordinary array O(n),dynamic array O(n)
             Optimized Search - ordinary array O(log n),dynamic array O(log n)
             Input - ordinary array (not allowed),dynamic array O(n)
            */
            #endregion
            #region LinkedLists
            /*           
              Data stores in nodes
              node stores one element and reference to next node
              linked lists connect nodes using references from one node to next
              Linked lists are very optimized for input or delete, but very slow for indexing and
              searching
            */
            LinkedList<int> linkedList = new LinkedList<int>();
            linkedList.AddFirst(13);//adding from head
            linkedList.AddLast(16);//adding from end
            var newNode = linkedList.AddLast(3);
            linkedList.AddAfter(newNode, 123);
            /*
             * complexity
             * LinkedList
             * indexing - O(n)
             * search - O(n)
             * optimized search - O(n)
             * input - O(1)
             */
            #endregion
            #region Stack
            /*
             * usually implemented with linked lists or arrays 
             * Stack is LIFO based data structure (last in , first out)
             * head of the linked list, lying on base of structe is the only way to to add or delete item
             */
            Stack<int> stack = new Stack<int>();
            stack.Push(12);
            stack.Push(11);
            stack.Push(10);
            stack.Push(9);
            stack.Push(15); //add head of stack
            stack.Peek();// show head of stack without removing
            stack.Pop();// show head of stack with removing
            #endregion
            #region Queue
            /*ques FIFO- Typed Data Structure
              que can be implemetd by linked lists or arrays
              que is like DoubleLinkedList , you can add element from end and remove from head
             */
            Queue<int> que = new Queue<int>();
            que.Enqueue(16);
            que.Enqueue(10); // Add to queue
            que.Enqueue(5);
            que.Enqueue(14);

            que.Dequeue(); // remove from queue

            var queueFirst = que.Peek();

            /*comlexity
             indexing - O(n)
             Search - O(n)
             Optimezd search - O(n)
             insert - O(1)
             */
            #endregion
            #region Hashed-Table
            /*
                Data stored in pair key-value
                hash-function taking key and giving the value relevant to that key only
                hesh-functions returning unique reference for datas
                hash-Functions are developed fr optimized search,input and deleteing
                hashes are inportant for associative arrays and indexing withouts data base
             */
            Hashtable ht = new Hashtable();
            ht.Add("number1", 12);
            ht.Add("number2", 14);
            ht.Add("number3", 13);
            /*try
            {
                ht.Add("number3", 13);
            }
            catch
            {
                Console.WriteLine("key Already exists");//hashtable throws exeption if
                                                        //the key already exists
            }
            foreach (DictionaryEntry de in ht)
            {
                Console.WriteLine("Key = {0}, Value = {1}", de.Key, de.Value);
            }*/
            /*
             * complexity
             Indexing - O(1)
             search - O(1)
             input - O(1)
            */
            #endregion

            #region Binary Tree
            /*binary tree is that type of structure that every node have max 2 child element 
             child elements can be left and right
             tree is made for lists and sorting optimization 
             if the tree is one-sided then it's just a LinkedList
             trees are relatively easy to implement comapred  to other data structures
             trees are used for implementing binary search

             tree decides where to move by comparing their keys
             right child node key is less then parent node key, left node key is grater than parent node key 
             */
            BinaryTree<int> bt = new BinaryTree<int>(BinaryTree<int>.CompareFunc_Int);
            bt.Add(5);
            bt.Add(15);
            bt.Add(25);
            bt.Add(92);
            bt.Add(12);
            bt.Add(4);
            bt.Add(6);
            bt.Add(13);
            foreach (var item in bt)
            {
                Console.WriteLine(item);
            }


            /*complexity 
             indexing - O(log n)
             search - O(log n)
             inserting - O(log n)
             */
            #endregion
            #region Testing

            //Random rd = new Random();
            //int[] arr = new int[7];
            //for (int i = 0; i < arr.Length; i++)
            //{
            //    arr[i] = rd.Next(1, 10);
            //}
            //Sorting<int> sort = new Sorting<int>();
            ///* sort.BubbleArray(arr);
            //    foreach (int x in arr)
            // {
            //  Console.WriteLine(x);
            // }*/
            //sort.QuickSort(arr, 0, arr.Length - 1);
            //foreach (int x in arr)
            //{
            //    Console.WriteLine(x);
            //}
            #endregion
        }
    }
}

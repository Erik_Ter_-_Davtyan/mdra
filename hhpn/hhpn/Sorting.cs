﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hhpn
{
    class Sorting<T> where T : IComparable
    {
        public Comparison<T> compareFunc;
        public void BubbleArray(T[] arr) 
        {
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i; j > 0; j--)
                {
                    int comperision = arr[j].CompareTo(arr[j - 1]);
                    if (comperision < 0)
                    {
                        Swap(ref arr[j], ref arr[j - 1]);
                    }
                }
            }
        }
        public void QuickSort(T[] items, int left, int right)
        {
            int i = left;
            int j = right;
            IComparable pivot = items[left];
            while (i <= j)
            {
                for (; (items[i].CompareTo(pivot) < 0) && (i.CompareTo(right) < 0); i++) ;
                for (; (pivot.CompareTo(items[j]) < 0) && (j.CompareTo(left) > 0); j--) ;
                if (i <= j)
                    Swap(ref items[i++], ref items[j--]);
            }
            if (left < j)
                QuickSort(items, left, j);
            if (i < right)
                QuickSort(items, i, right);
        }
        static void Swap(ref T x, ref T y)
        {
            T temp = x;
            x = y;
            y = temp;
        }


        static public void Merge(T[] arr, int p, int q, int r)
        {
            int i, j, k;
            int n1 = q - p + 1;
            int n2 = r - q;
            T[] L = new T[n1];
            T[] R = new T[n2];
            for (i = 0; i < n1; i++)
            {
                L[i] = arr[p + i];
            }
            for (j = 0; j < n2; j++)
            {
                R[j] = arr[q + 1 + j];
            }
            i = 0;
            j = 0;
            k = p;
            while (i < n1 && j < n2)
            {
                if (L[i].CompareTo(R[j]) <=0)
                {
                    arr[k] = L[i];
                    i++;
                }
                else
                {
                    arr[k] = R[j];
                    j++;
                }
                k++;
            }
            while (i < n1)
            {
                arr[k] = L[i];
                i++;
                k++;
            }
            while (j < n2)
            {
                arr[k] = R[j];
                j++;
                k++;
            }
        }
        static public void MergeSort(T[] arr, int p, int r)
        {
            if (p < r)
            {
                int q = (p + r) / 2;
                MergeSort(arr, p, q);
                MergeSort(arr, q + 1, r);
                Merge(arr, p, q, r);
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hhpn
{
    class BinaryTree<T>
    {
        public int Count { get; set; }
        class BinaryTreeNode
        {
            public BinaryTreeNode Left;
            public BinaryTreeNode Right;
            public BinaryTreeNode Parent;
            public T Data;
            public BinaryTreeNode()
            {
                Left = null;
                Right = null;
                Parent = null;
            }
        }
        BinaryTreeNode Root;
        Comparison<T> CompareFunc;
        public BinaryTree(Comparison<T> cf)
        {
            Root = null;
            CompareFunc = cf;
        }
        public static int CompareFunc_Int(int left, int right)
        {
            return left - right;
        }
        public static int CompareFunc_string(string left, string right)
        {
            return left.CompareTo(right);
        }
        public void Add(T value)
        {
            BinaryTreeNode child = new BinaryTreeNode();
            child.Data = value;
            if (Root == null)
            {
                Root = child;
                Count++;
            }
            else
            {
                BinaryTreeNode Iterator = Root;
                while (true)
                {
                    int compare = CompareFunc(value, Iterator.Data);
                    if (compare <= 0)
                    {
                        if (Iterator.Left != null)
                        {
                            Iterator = Iterator.Left;
                            continue;
                        }
                        else
                        {
                            Iterator.Left = child;
                            child.Parent = Iterator;
                            Count++;
                            break;
                        }
                    }
                    if (compare > 0)
                    {
                        if (Iterator.Right != null)
                        {
                            Iterator = Iterator.Right;
                            continue;
                        }
                        else
                        {
                            Iterator.Right = child;
                            child.Parent = Iterator;
                            Count++;
                            break;
                        }
                    }
                }
            }
        }
        public bool Find(T value)
        {
            BinaryTreeNode Iterator = Root;
            while (Iterator != null)
            {
                int compare = CompareFunc(value, Iterator.Data);
                if (compare == 0)
                {
                    return true;
                }
                if (compare < 0)
                {
                    Iterator = Iterator.Left;
                    continue;
                }
                Iterator = Iterator.Right;
            }
            return false;
        }
        BinaryTreeNode FindMostLeft(BinaryTreeNode start)
        {
            BinaryTreeNode node = start;
            while (true)
            {
                if (node.Left != null)
                {
                    node = node.Left;
                    continue;
                }
                break;
            }
            return node;
        }
        public IEnumerator<T> GetEnumerator()
        {
            return new BinaryTreeEnumerator(this);
        }
        class BinaryTreeEnumerator : IEnumerator<T>
        {
            BinaryTreeNode current;
            BinaryTree<T> theTree;
            public BinaryTreeEnumerator(BinaryTree<T> tree)
            {
                theTree = tree;
                current = null;
            }
            public bool MoveNext()
            {
                if (current == null)
                {
                    current = theTree.FindMostLeft(theTree.Root);
                }
                else
                {
                    if (current.Right != null)
                        current = theTree.FindMostLeft(current.Right);
                    else
                    {

                        T currentValue = current.Data;
                        while (current != null)
                        {
                            current = current.Parent;
                            if (current != null)
                            {
                                int compare = theTree.CompareFunc(current.Data, currentValue);
                                if (compare < 0)
                                    continue;
                            }
                            break;
                        }
                    }

                }
                return (current != null);
            }
            public T Current
            {
                get
                {
                    if (current == null)
                        throw new InvalidOperationException();
                    return current.Data;
                }
            }
            object IEnumerator.Current
            {
                get
                {
                    if (current == null)
                        throw new InvalidOperationException();
                    return current.Data;
                }
            }
            public void Dispose() { }
            public void Reset() { current = null; }
        }
    }
}

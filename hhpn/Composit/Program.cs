﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
    class Program
    {
        static void Main(string[] args)
        {
            Composite root = new Composite("root");
            root.Add(new Leaf("leaf1"));
            root.Add(new Leaf("leaf2"));

            Composite composite = new Composite("CompositeX");

            composite.Add(new Leaf("LeafX1"));
            composite.Add(new Leaf("LeafX2"));
            root.Add(composite);
            root.Add(new Leaf("Leaf C"));

            //Remove from a leaf
            Leaf leaf = new Leaf("Leaf D");
            root.Add(leaf);
            root.Remove(leaf);

            //recurcive display
            root.Display(1);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
    /// <summary>
    /// represents leaf objects in the composition. A leaf has no children.
    /// defines behavior for primitive objects in the composition.
    /// </summary>
    class Leaf : Component
    {
        public override void Add(Component c)
        {
            Console.WriteLine("cannot add to leaf");
        }
        public Leaf(string name)
            :base(name)
        {

        }
        public override void Display(int depth)
        {
            Console.WriteLine(new String('-',depth) + name);
        }

        public override void Remove(Component c)
        {
            Console.WriteLine("cannot delete from leaf");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Composite
{
    /// <summary>
    /// defines behavior for components having children.
    ///stores child components.
    /// implements child-related operations in the Component interface.
    /// </summary>
        //Client ↓
        //manipulates objects in the composition through the Component interface.
    class Composite : Component
    {
        private List<Component> children = new List<Component>();
        public Composite(string name) : base(name)
        {
        }

        public override void Add(Component c)
        {
            children.Add(c);
        }


        public override void Display(int depth)
        {
            Console.WriteLine(new String('-', depth) + name);
            foreach (var item in children)
            {
                item.Display(depth + 2);
            }
        }
        public override void Remove(Component c)
        {
            children.Remove(c);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    class SortedList
    {
        private List<string> _list;
        private SortStrategy _sortStrategy;
        public void SetSortStratedgy(SortStrategy strategy)
        {
            this._sortStrategy = strategy;
        }
        public void Add(string name)
        {
            _list.Add(name);
        }
        public void Sort()
        {
            _sortStrategy.Sort(_list);
            foreach (var item in _list)
            {
                Console.WriteLine(" " + item);
            }
            Console.ReadLine();
        }
    }
}

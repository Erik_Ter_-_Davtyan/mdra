﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sortedList = new SortedList();

            sortedList.Add("Samual");
            sortedList.Add("Jimmy");
            sortedList.Add("Sandra");
            sortedList.Add("Vivek");
            sortedList.Add("Anna");

            sortedList.SetSortStratedgy(new QuickSort());
            sortedList.Sort();

            sortedList.SetSortStratedgy(new ShellSort());
            sortedList.Sort();

            sortedList.SetSortStratedgy(new MergeSort());
            sortedList.Sort();
        }
    }
}

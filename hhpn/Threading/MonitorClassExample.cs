﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Threading
{
    class MonitorClassExample
    {

        public void MonitorMain()
        {
            List<Task> tasks = new List<Task>();
            Random rd = new Random();
            long total = 0;
            int n = 0;
            for (int i = 0; i < 10; i++)
            {
                tasks.Add(Task.Run(() =>
                {
                    int[] values = new int[10000];
                    int taskTotal = 0;
                    int taskN = 0;
                    int ctr = 0;
                    Monitor.Enter(rd);
                    for (; ctr < 10000; ctr++)
                    {
                        values[ctr] = rd.Next(0, 1001);
                    }
                    Monitor.Exit(rd);
                    taskN = ctr;
                    foreach (var item in values)
                        taskTotal += item;
                    Console.WriteLine("Mean task for {0,2}: {1:N2} (N = {2:N0})", Task.CurrentId,
                        (taskTotal * 1.0) / taskN, taskN);
                    Interlocked.Add(ref n, taskN);
                    Interlocked.Add(ref total, taskTotal);
                }));

            }
            try
            {
                Task.WaitAll(tasks.ToArray());
                Console.WriteLine("\nMean for all tasks: {0:N2} (N={1:N0})",
                       (total * 1.0) / n, n);
            }
            catch (AggregateException e)
            {
                foreach (var ie in e.InnerExceptions)
                    Console.WriteLine("{0}: {1}", ie.GetType().Name, ie.Message);
                throw;
            }
        }

    }
}

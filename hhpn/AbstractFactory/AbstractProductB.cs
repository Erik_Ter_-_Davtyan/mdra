﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    //declares an interface for a type of product object
    abstract class AbstractProductB
    {
        public abstract void Interact(AbstractProductA a);
    }
}

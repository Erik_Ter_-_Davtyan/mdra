﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// A single class that represents an entire subsystem
/// </summary>
namespace Facade
{
    /// <summary>
    /// Provide a unified interface to a set of interfaces in a subsystem. 
    /// Façade defines a higher-level interface that makes the subsystem easier to use.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Facade fcd = new Facade();
            fcd.MethodA();
            fcd.MethodB();
        }
    }
}

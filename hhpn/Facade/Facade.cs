﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    /// <summary>
    /// knows which subsystem classes are responsible for a request.
    ///delegates client requests to appropriate subsystem objects.
    /// </summary>
    class Facade
    {
        private SubSystemOne _one;
        private SubSystemTwo _two;
        private SubSystemThree _three;
        private SubSystemFour _four;

        public Facade()
        {
            _one = new SubSystemOne();
            _two = new SubSystemTwo();
            _three = new SubSystemThree();
            _four = new SubSystemFour();
        }
        public void MethodA()
        {
            Console.WriteLine("----------Method A---------");
            _one.MethodOne();
            _two.MethodTwo();
        }
        public void MethodB()
        {
            Console.WriteLine("----------Method A---------");
             lock (this)
            {
                _one.MethodOne();
                _two.MethodTwo();
                _four.MethodFour();
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    /// <summary>
    /// Define an interface for creating an object, but let subclasses decide which class to 
    /// instantiate. Factory Method lets a class defer instantiation to subclasses.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Creator[] creators = new Creator[2];
            creators[0] = new ConcreteCreatorA();
            creators[1] = new ConcreteCreatorB();

            foreach  (Creator item in creators)
            {
                Product product = item.FactoryMethod();
                Console.WriteLine($"Created {product.GetType().Name}");
            }
        }
    }
}

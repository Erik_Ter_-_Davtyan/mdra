﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class ConcreteProductB:Product
    {
        public string P_Name { get; set; }
        public string P_Desc { get; set; }
        public string P_Price { get; set; }
    }
}

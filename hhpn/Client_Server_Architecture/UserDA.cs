﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Client_Server_Architecture;

namespace Client_Server_Architecture
{
    public class UserDA
    {
        static Config conf = new Config();
        SqlConnection cnn = new SqlConnection(conf.ConnString);
        public int AddUserDetails(UserBO boObj)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sprocUserinfoInsertSingleItem",cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name",boObj.Name);
                cmd.Parameters.AddWithValue("@Address",boObj.Addres);
                cmd.Parameters.AddWithValue("@Email",boObj.Email);
                cmd.Parameters.AddWithValue("@PhoneNumber",boObj.PhoneNumber);
                cnn.Open();
                int result = cmd.ExecuteNonQuery();
                cnn.Close();
                if (result > 0)
                {
                    return result;
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Something went Wrong" + new InvalidOperationException());
                return -1;
            }
        }
    }
}

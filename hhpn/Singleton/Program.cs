﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Ensure a class has only one instance and provide a global point of access to it.
/// </summary>
namespace Singleton
{
    class Program
    {

        static void Main(string[] args)
        {
            Singleton sl1 = Singleton.Instance();
            Singleton sl2 = Singleton.Instance();


            if (sl1 == sl2)
            {
                Console.WriteLine("Objects are the same instance");
            }
            // output is "Objects are the same instance"
        }
    }
}

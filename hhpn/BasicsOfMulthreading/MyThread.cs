﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace BasicsOfMulthreading
{
    class MyThread
    {

        private object threadLock = new object();
        public void ThreadNumbers()
        {
            lock (this)
            {
                Console.WriteLine("{0} Thread uses ThreadNumbers method", Thread.CurrentThread.Name);
                Console.Write("Numbers");
                Random rd = new Random();
                for (int i = 0; i < 10; i++)
                {
                    Console.Write(i + "\t");
                }
            Console.WriteLine();
            }
        }
    }
}


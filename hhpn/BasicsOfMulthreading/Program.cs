﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace BasicsOfMulthreading
{
    class Program
    {
        static void Main(string[] args)
        {
            MyThread thread = new MyThread();
            Thread[] threads = new Thread[10];
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(new ThreadStart(thread.ThreadNumbers));
                threads[i].Name = string.Format("Works Thread #{0} + \a", i);
            }
            foreach (var item in threads)
            {
                item.Start();
            }
        }
    }
}

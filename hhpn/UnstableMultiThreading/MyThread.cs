﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace UnstableMultiThreading
{
    class MyThread
    {
        public void ThreadNumbers()
        {
            Console.WriteLine("{0} Thread uses ThreadNumbers method", Thread.CurrentThread.Name);
            Console.WriteLine("Numbers");
            Random rd = new Random();
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(1000 * rd.Next(5));
                Console.Write(i + "\t");
            }
            Console.WriteLine();
        }
    }
}

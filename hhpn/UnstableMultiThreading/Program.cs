﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace UnstableMultiThreading
{
    class Program
    {
        static void Main(string[] args)
        {
            MyThread mt = new MyThread();
            Thread[] threads = new Thread[10];
            for (int i = 0; i < 10; i++)
            {
                threads[i] = new Thread(new ThreadStart(mt.ThreadNumbers));
                threads[i].Name = string.Format("Works thread #{0}", i);
            }
            foreach (var item in threads)
            {
                item.Start();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonExample2
{
    class Server
    {
        public string Name { get; set; }
        public string IP { get; set; }
    }
}

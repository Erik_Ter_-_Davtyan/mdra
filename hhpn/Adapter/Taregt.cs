﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    //defines the domain-specific interface that Client uses.
    class Taregt
    {
        public virtual void Request()
        {
            Console.WriteLine("Called target Request()");
        }
    }
}

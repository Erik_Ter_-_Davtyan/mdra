﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// A way of notifying change to a number of classes
/// </summary>
namespace Observer
{
    /// <summary>
    /// Define a one-to-many dependency between objects so
    /// that when one object changes state, all its dependents are notified and updated automatically.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            ConcreteSubject subject = new ConcreteSubject();
            subject.Attach(new ConcreteObserver(subject, "X"));
            subject.Attach(new ConcreteObserver(subject, "Y"));
            subject.Attach(new ConcreteObserver(subject, "Z"));
            subject.SubjectState = "ABC";
            subject.Notify();
        }
    }
}

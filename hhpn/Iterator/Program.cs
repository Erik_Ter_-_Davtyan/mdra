﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Sequentially access the elements of a collection
/// </summary>
namespace Iterator
{
    /// <summary>
    /// Provide a way to access the elements of an aggregate 
    /// object sequentially without exposing its underlying representation.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            ConcreteAggregate a = new ConcreteAggregate();
            a[0] = "A";
            a[1] = "B";
            a[2] = "C";
            a[3] = "D";
            Iterator i = a.CreateIterator();
            Console.WriteLine("iterating over collection!");

            object item = i.First();
            while (item != null)
            {
                Console.WriteLine(item);
                item = i.Next();
            }

        }
    }
}
